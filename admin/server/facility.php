<?php
	
	include("public.php");

/**
 * config.php
 * 
 * 本地配置调用接口说明：
 * 1.初次配置时，需要修改下面的服务器地址
 * 2.port=getlist,显示列表;
 * 3.port=getinfo,显示信息详情,参数;ckey = <company_key>
 * 4.port=fix,修改数据,参数fix_id =<id>数据post表单数据
 * 5.port=add,添加数据,参数 无,数据:post表单数据
 */
		
	$port =(isset($_GET['port'])) ? $_GET['port'] : false ;
	
	switch ($port){

		/**
		 * 获取列表
		 * 
		 */
		case "getlist":
			$url = $site_domain.'facility.getlist';			
			$json_data = curlGet($url);
			print_r($json_data);							
			break;
			

		/**
		 * 获取详情
		 * @param string ckey 公司唯一授权key
		 */
		case "getinfo":	
		
			$mac = (isset($_GET['tv_mac'])) ? $_GET['tv_mac'] : false ;
			$id =  (isset($_GET['id'])) ? $_GET['id'] : false ;
		
			$url =$site_domain.'facility.getinfo&tv_mac=' . $mac . '&id=' . $id;					
			$json_data = curlGet($url);
			print_r($json_data);								
			break;



		/**
		 * 修改数据
		 * @param int id 修改数据的id号
		 */	
		case "fix":
			$data = file_get_contents("php://input");//获取表单提交的数据;		
			$fixid = (isset($_GET['id'])) ? $_GET['id'] : false ;		
			$url = $site_domain.'facility.fix&id='.$fixid;			
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据
			print_r($json_data);					
			break;
			
			
		/**
		 * 新增数据
		 */	
		case "add":
			$data = file_get_contents("php://input");//获取表单提交的数据;
			$url = $site_domain.'facility.add';
			
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据
			print_r($json_data);
					
			break;
			
			
		/**
		 * 删除数据
		 * @param int id 删除数据的id号
		 */
		case "del":
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;
			$url =$site_domain.'facility.del&id=' . $id;
			
			$json_data = curlGet($url);
			$arr = json_decode($json_data,true);
			if(is_array($arr)){
				$success['referer'] = '';
				$success['refresh'] = true;
				$success['state'] 	= 'success';
				$success['message'] = '删除成功';
				$jsonnew =	json_encode(array_merge($arr,$success));
			}
		
			print_r($jsonnew);
						
			break;
				


		default:
			print_r(json_encode($fail));
			break;
	}

?>