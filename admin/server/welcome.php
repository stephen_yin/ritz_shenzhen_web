<?php
	
	include("public.php");

	/**
	 * welcome.php
	 */
		
	$port =(isset($_GET['port'])) ? $_GET['port'] : false ;
	
	switch ($port){

		case "getinfo" :		
		
			$requestTime = date('Y-m-d H:i:s',$_SERVER['REQUEST_TIME']);//得到请求此php脚本时的时间戳
			$cache = new Cache();  
			$expiration = 86400;
			if($lang == "en") $json_data = $cache->get('welcome_en',$expiration);
			else			  $json_data = $cache->get('welcome_cn',$expiration);
			if ($json_data === FALSE){
				$url =$site_domain.'welcome.getinfo&id=1';
				$json_data = curlGet($url);
				if($lang == "en")	$cache->set('welcome_en', $json_data); 
				else 				$cache->set('welcome_cn', $json_data); 
				file_put_contents('./log/log.txt','['.$requestTime.']--欢迎页访问了一次接口数据  | ',FILE_APPEND);
			
			}		      
			print_r($json_data);					
			break;
			
		case "fix" :
			$data = file_get_contents("php://input");//获取表单提交的数据;		
			$url = $site_domain.'welcome.fix&id=1';		
			$json_data = curlPost($url,$data);
			$cache = new Cache(); 
			if($lang == "en") $cache->clear('welcome_en');
			else $cache->clear('welcome_cn');		
			print_r($json_data);
			break;
			
		default:
			print_r(json_encode($fail));
	}

?>