<?php
include 'public.php';
$port = isset($_GET['port']) ? $_GET['port'] : null;
switch ($port) {
	/**
	 * 获取记录总数
	 */
    case "getlistcount":
        $url = $site_domain . 'tv.getlistcount';
        $json_data = curlGet($url);
        print_r($json_data);
        break;
	/**
	 * 获取记录总数
	 */	
    case "getlist":
        $tv_mac = isset($_GET['tv_mac']) ? $_GET['tv_mac'] : false;
        if ($tv_mac) {
            $url = $site_domain . 'tv.getlist&tv_mac=' . $tv_mac;
        } else {
            $url = $site_domain . 'tv.getlist';
        }
        $json_data = curlGet($url);
        print_r($json_data);
        break;
	/**
	 * 获取记录总数
	 */	
    case "getlist_admin":
        $from = isset($_GET['from']) ? $_GET['from'] : null;
        $nums = isset($_GET['nums']) ? $_GET['nums'] : null;
        $url = $site_domain . 'tv.getlist_admin&from=' . $from . '&nums=' . $nums;
        $json_data = curlGet($url);
        print_r($json_data);
        break;
	/**
	 * 获取记录总数
	 */	
    case "search":
        $keywords = isset($_GET['keywords']) ? $_GET['keywords'] : null;
        $key = urlencode($keywords);
        if ($keywords) {
            $url = $site_domain . 'tv.search&keywords=' . $key;
            $json_data = curlGet($url);
            print_r($json_data);
        }
        break;
	/**
	 * 获取指定频道信息
	 */	
    case "getinfo":
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $channel_number = isset($_GET['channel_number']) ? $_GET['channel_number'] : null;
        $url = $site_domain . 'tv.getinfo&id=' . $id;
        $json_data = curlGet($url);
        print_r($json_data);
        break;
	/**
	 * 修改指定频道
	 */	
    case "fix":
        $data = file_get_contents("php://input");
        //获取表单提交的数据;
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $url = $site_domain . 'tv.fix&id=' . $id;
        $json_data = curlPost($url, $data);
        //调用curlPost的方法，获取API返回的数据
        print_r($json_data);
        break;
	/**
	 * 检查频道序号是否存在
	 */	
    case "check":
        $channel_order = isset($_GET['channel_order']) ? $_GET['channel_order'] : null;
        $url = $site_domain . 'tv.checkbyorder&channel_order=' . $channel_order;
        $json_data = curlGet($url);
        print_r($json_data);
        break;
	/**
	 * 增加新的频道
	 */	
    case "add":
        $data = file_get_contents("php://input");
        //获取表单提交的数据;
        $url = $site_domain . 'tv.add';
        $json_data = curlPost($url, $data);
        //调用curlPost的方法，获取API返回的数据
        print_r($json_data);
        break;
	/**
	 * 删除指定记录
	 */	
    case "del":
        $id = isset($_GET['id']) ? $_GET['id'] : false;
        $url = $site_domain . 'tv.del&id=' . $id;
        $json_data = curlGet($url);
        $arr = json_decode($json_data, true);
        if (is_array($arr)) {
            $success['referer'] = '';
            $success['refresh'] = true;
            $success['state'] = 'success';
            $success['message'] = '删除成功';
            $jsonnew = json_encode(array_merge($arr, $success));
        }
        print_r($jsonnew);
        break;
	/**
	 * 全部删除
	 */	
    case "delall":
        $id = isset($_GET['id']) ? $_GET['id'] : false;
        $url = $site_domain . 'tv.delall&dellall=true';
        $json_data = curlGet($url);
        print_r($json_data);
        break;
	/**
	 * 默认返回错误
	 */	
    default:
        print_r(json_encode($fail));
}