<?php
/**
 * Emlog图片验证码生成程序
 * @copyright (c) Emlog All Rights Reserved
 * Modify By Mrxn 
 * Emlog  Site: http://www.emlog.net/
 * Mrxn's Blog: http://www.mrxn.net/
 */

session_start();

$randCode = '';
$chars = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPRSTUVWXYZ23456789';
for ( $i = 0; $i < 5; $i++ ){
        $randCode .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
}

$_SESSION['code'] = strtoupper($randCode);

$img = imagecreate(75,25) or die("创建图像资源失败,请刷新页面");
$bgColor = isset($_GET['mode']) && $_GET['mode'] == 't' ? imagecolorallocate($img,245,245,245) : imagecolorallocate($img,255,255,255);
$pixColor = imagecolorallocate($img,mt_rand(88, 245), mt_rand(55, 240), mt_rand(99, 200));
//画字符、大小
for($i = 0; $i < 5; $i++){
        $x = $i * 13 + mt_rand(3, 7) - 2;
        $y = mt_rand(0, 3);
        $text_color = imagecolorallocate($img, mt_rand(100, 250), mt_rand(80, 180), mt_rand(90, 220));
        imagechar($img, 5, $x + 5, $y + 3, $randCode[$i], $text_color);
}
//画干扰点
for($j = 0; $j < 240; $j++){
        $x = mt_rand(0,100);
        $y = mt_rand(0,40);
        imagesetpixel($img,$x,$y,$pixColor);
}
//////4条横斜线
//for ($i=0; $i < 5; $i++) { 
//  $lineColor = imagecolorallocate($img, rand(50, 150), rand(50, 150), rand(50, 150));
//  $lineX1 = 0;
//  $lineX2 = 80;
//  $lineY1 = ($i + 1) * 8;
//  $lineY2 = ($i + 1) * 15;
//  imageline($img, $lineX1, $lineY1, $lineX2, $lineY2, $lineColor);
//}
//////4条竖斜线
//for ($i=0; $i < 5; $i++) { 
//  $lineColor = imagecolorallocate($img, rand(50, 150), rand(50, 150), rand(50, 150));
//  $lineY1 = 0;
//  $lineY2 = 30;
//  $lineX1 = ($i + 1) * 8;
//  $lineX2 = ($i + 1) * 15;
//  imageline($img, $lineX1, $lineY1, $lineX2, $lineY2, $lineColor);
//}

header('Content-Type: image/png');
imagepng($img);
imagedestroy($img);
?>