<?php

	include('public.php');

/**
 * login.php
 * 
 * 本地配置调用接口说明：
 * 1.初次配置时，需要修改下面的服务器地址
 * 2.port=getlist,显示列表;
 * 3.port=getinfo,显示信息详情,参数;ckey = <company_key>
 * 4.port=fix,修改数据,参数fix_id =<id>数据post表单数据
 * @pram 5.port=add,添加数据,参数 无,数据:post表单数据
 * http://localhost/IO/Public/ritzcarlton/?service=Guest.add
 */


	$port =(isset($_GET['port'])) ? $_GET['port'] : false ;
	
	switch ($port){
		
		case "add" :
			$data = file_get_contents("php://input");//获取表单提交的数据;
			$url = $site_domain.'Guest.add';	
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据/
			print_r($json_data);				
			break;
			
		case "fix" :
			$data = file_get_contents("php://input");//获取表单提交的数据;
			$url = $site_domain.'Guest.fix';	
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据/
			print_r($json_data);				
			break;
			
		case "getinfo" :
			$roomId =(isset($_GET['room_id'])) ? $_GET['room_id'] : false ;	
	
			$url = $site_domain.'Guest.getinfo&room_id=' . $roomId;	
			$json_data = curlGet($url);//调用curlPost的方法，获取API返回的数据/
			print_r($json_data);				
			break;
		case "del" :
			$roomId =(isset($_GET['room_id'])) ? $_GET['room_id'] : false ;	
	
			$url = $site_domain.'Guest.del&room_id=' . $roomId;	
			$json_data = curlGet($url);//调用curlPost的方法，获取API返回的数据/
			print_r($json_data);				
			break;

		default:
			
			print_r(json_encode($fail));
	}
		

?>