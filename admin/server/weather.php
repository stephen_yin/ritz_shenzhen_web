<?php
include('public.php');



$port =(isset($_GET['port'])) ? $_GET['port'] : false ;

if($port == 'getlist'){
	
	$requestTime = date('Y-m-d H:i:s',$_SERVER['REQUEST_TIME']);//得到请求此php脚本时的时间戳
	$cache = new Cache();  
	$expiration = 26400;
	if($lang == "en") $json_data = $cache->get('weatheren',$expiration);
	else			  $json_data = $cache->get('weather',$expiration);

	
	if ($json_data === false){
		$url = $site_domain .  'weather.getlist';			
		$json_data = curlGet($url);
		if($lang == "en")	$cache->set('weatheren', $json_data); 
		else 				$cache->set('weather', $json_data); 
		file_put_contents('./log/log.txt','['.$requestTime.']--访问了一次接口数据  | ',FILE_APPEND);
	
	}		      
	print_r($json_data);
}


?>