<?php

	include('public.php');


	$port =(isset($_GET['port'])) ? $_GET['port'] : false ;
	
	switch ($port){
		
		case "getlistcount" :
			$url = $site_domain.'mpi.getlistcount';
			$json_data = curlGet($url);
			print_r($json_data);			
			break;


		case "getlist" :
			$from = (isset($_GET['from'])) ? $_GET['from'] : null ;//分页参数 第一页为0
			$nums = (isset($_GET['nums'])) ? $_GET['nums'] : null ;//分页参数 每页显示数量					
			$url = $site_domain.'mpi.getlist&from='.$from.'&nums='.$nums;
			$json_data = curlGet($url);
			print_r($json_data);					
			break;


		case "search" :
			$keywords = (isset($_GET['keywords'])) ? $_GET['keywords'] : null ;
			$key = urlencode($keywords);
			if($keywords){
				$url = $site_domain.'mpi.search&keywords='.$key;				
				$json_data = curlGet($url);
				print_r($json_data);				
			}
			break;



		case "getinfo" :
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;
			$url =$site_domain.'mpi.getinfo&id=' . $id;			
			$json_data = curlGet($url);
			print_r($json_data);						
			break;


		case "fix" :
			$data = file_get_contents("php://input");//获取表单提交的数据;	
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;		
			$url = $site_domain.'mpi.fix&id=' . $id;			
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据
			print_r($json_data);			
			break;
			
		case "fixstatus" :			
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;	
			$msg_status = 	(isset($_GET['msg_status'])) ? $_GET['msg_status'] : false ;	
			$url = $site_domain.'mpi.fixstatus&id=' . $id .'&msg_status=' . $msg_status;	
			$json_data = curlGet($url);
			print_r($json_data);				
			break;


		case "add" :
			$data = file_get_contents("php://input");//获取表单提交的数据;
			$url = $site_domain.'mpi.add';			
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据
			print_r($json_data);			
			break;


		case "del" :
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;
			$url =$site_domain.'mpi.del&id=' . $id;			
			$json_data = curlGet($url);
			print_r($json_data);					
			break;	

		case "delall" :
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;
			$url =$site_domain.'mpi.delall&dellall=true';			
			$json_data = curlGet($url);
			print_r($json_data);					
			break;	
	



		default:
			
			print_r(json_encode($fail));
	}
		

?>