<?php
	ini_set("display_errors", "On");
	error_reporting(E_ALL | E_STRICT);
	header("Content-type: text/html; charset=utf-8"); 
	date_default_timezone_set('PRC'); 
	$port =(isset($_GET['port'])) ? $_GET['port'] : false ;
	$lang =(isset($_GET['lang'])) ? $_GET['lang'] : null ;
//	<meta name="referrer" content="no-referrer|no-referrer-when-downgrade|origin|origin-when-crossorigin|unsafe-url">
//	header('Access-Control-Allow-Origin: *');
//	header('Access-Control-Allow-Origin: 127.0.0.1');   
	$Sign = "5e64192203c0293c48ab7108e95700cd";
//	$site_domain = "http://kf.lp44.cn/PhalApi/Public/iptv/?sign=".$Sign."&service=";//(调试服务器1.34)
//	$site_domain = "http://kf.lp44.cn/IO/Public/ritzcarlton/?service=";//(调试服务器1.4)
	
	$runMode = "debug"; //demo演示文件; debug 调试模式; local 本地模式; online 在线模式(site_domain地址必须是外网地址);
	// $site_domain = "http://192.168.10.231/PhalApi/Public/iptv/?sign=".$Sign."&service=";//(本地开发服务器1.34)
	$apiServer = 'http://localhost';
	if($lang == "en")
		$site_domain = $apiServer . "/IO/Public/ritzcarltonen/?lang=en&service=";//(本地开发服务器1.4)
	else
		$site_domain = $apiServer . "/IO/Public/ritzcarlton/?service=";//(本地开发服务器1.4)

/**
 * 自定义返回数据给ajaxForm.js提供callback显示;
 */
	$fail = array();
	$fail['code'] = '501';		
	$fail['referer'] = '';
	$fail['refresh'] = true;
	$fail['state'] = 'fail';
	$fail['message'] = '缺少指定的参数！';

	$success = array();		
	$success['referer'] = '';
	$success['refresh'] = true;
	$success['state'] = 'success';
	$success['message'] = '提交成功';

/** **************************************************************************************************************************************
 * curl Get
 * 
 * @param   string  url 
 * @param   array   数据 
 * @param   int     请求超时时间 
 * @param   bool    HTTPS时是否进行严格认证 
 * @return  string 
 */  
function curlGet($url,$timeout = 15){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	//设置curl默认访问为IPv4
	if(defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')){
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
	}
	//设置curl请求连接时的最长秒数，如果设置为0，则无限
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout-2);
	//设置curl总执行动作的最长秒数，如果设置为0，则无限
	curl_setopt ($ch, CURLOPT_TIMEOUT,$timeout);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    // 要求结果为字符串且输出到屏幕上
	curl_setopt($ch, CURLOPT_HEADER, 0); // 不要http header 加快效率
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);//超时设置
  	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 对认证证书来源的检查，0表示阻止对证书的合法性的检查。             
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);   // 从证书中检查SSL加密算法是否存在 
//	curl_setopt($ch,CURLOPT_COOKIEJAR,$cookie_file);
    $ret = curl_exec($ch);
	// 检查是否有错误发生
	if(curl_errno($ch))
	{
//	    echo 'Curl error: ' . curl_error($ch);
		$err = curl_error($ch);  //查看报错信息  /	
		$fail = array();
		$fail['ret'] = '-887';
		$fail['msg'] = $err;
		$fail['message'] ='发生了一个错误！';
		return (json_encode($fail) );	
	}  
    curl_close($ch); 
	$arr = json_decode($ret,true);
	if(is_array($arr)){
		$success['referer'] = '';
		$success['refresh'] = true;
		$success['state'] 	= 'success';
		$success['message'] = 'success';
		$jsonnew =	json_encode(array_merge($arr,$success));
	}	
	return $jsonnew;
	

}  
	
	
	/** 
 * curl POST 
 * 
 * @param   string  url 
 * @param   array   数据 
 * @param   int     请求超时时间 
 * @param   bool    HTTPS时是否进行严格认证 
 * @return  string 
 */  
function curlPost($url, $data = array(), $timeout = 15){ 
      
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL, $url);  
    //设置curl默认访问为IPv4
	if(defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')){
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
	}
	//设置curl请求连接时的最长秒数，如果设置为0，则无限
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout-2);
	//设置curl总执行动作的最长秒数，如果设置为0，则无限
	curl_setopt ($ch, CURLOPT_TIMEOUT,$timeout);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书  
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // 检查证书中是否设置域名  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:')); //避免data数据过长问题  
    curl_setopt($ch, CURLOPT_POST, true);  
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
//	curl_setopt($ch,CURLOPT_COOKIEJAR,$cookie_file); 
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Connection: close"));
    //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); //data with URLEncode  
  
    $ret = curl_exec($ch);  
	
 	// 检查是否有错误发生
	if(curl_errno($ch))
	{
		$err = curl_error($ch);  //查看报错信息  /		
		$fail = array();
		$fail['ret'] = '-887';
		$fail['data'] = $err;
		$fail['message'] = $err;
		return (json_encode($fail) );
	}  

 	curl_close($ch); 	   
	$arr = json_decode($ret,true);
	if(is_array($arr)){		
		if($arr['ret'] != 1){
			$fail['ret'] =$arr['ret'];					
			$fail['data'] = '';
			$fail['msg'] = $arr['msg'];	
			$fail['code']= $arr['ret'];
			$fail['message'] = $arr['msg'];
			$jsonnew =	json_encode(array_merge($arr,$fail));
		}else{
			$success['referer'] = '';
			$success['refresh'] = true;
			$success['state'] 	= 'success';
			$success['message'] = 'success';
			$jsonnew =	json_encode(array_merge($arr,$success));
		}
		return $jsonnew;
	}	
	
}  


  
class Cache {  
  
    private $dir = './cache/';  
    private $expiration = 3600;  
  
    function __construct($dir = '')  
    {  
        if(!empty($dir)){ $this->dir = $dir; }  
    }  
  
    private function _name($key)  
    {  
        return sprintf("%s/%s", $this->dir, sha1($key));  
    }  
  
    public function get($key, $expiration = '')  
    {  
  
        if ( !is_dir($this->dir) OR !is_writable($this->dir))  
        {  
            return FALSE;  
        }  
  
        $cache_path = $this->_name($key);  
  
        if (!@file_exists($cache_path))  
        {  
            return FALSE;  
        }  
  
        $expiration = empty($expiration) ? $this->expiration : $expiration;  
  
        if (filemtime($cache_path) < (time() - $expiration))  
        {  
            $this->clear($key);  
            return FALSE;  
        }  
  
        if (!$fp = @fopen($cache_path, 'rb'))  
        {  
            return FALSE;  
        }  
  
        flock($fp, LOCK_SH);  
  
        $cache = '';  
  
        if (filesize($cache_path) > 0)  
        {  
            $cache = unserialize(fread($fp, filesize($cache_path)));  
        }  
        else  
        {  
            $cache = NULL;  
        }  
  
        flock($fp, LOCK_UN);  
        fclose($fp);  
  
        return $cache;  
    }  
  
    public function set($key, $data)  
    {  
  
        if ( !is_dir($this->dir) OR !is_writable($this->dir))  
        {  
             $this->_makeDir($this->dir);  
        }  
  
        $cache_path = $this->_name($key);  
  
        if ( ! $fp = fopen($cache_path, 'wb'))  
        {  
            return FALSE;  
        }  
  
        if (flock($fp, LOCK_EX))  
        {  
            fwrite($fp, serialize($data));  
            flock($fp, LOCK_UN);  
        }  
        else  
        {  
            return FALSE;  
        }  
        fclose($fp);  
        @chmod($cache_path, 0777);  
        return TRUE;  
    }  
  
    public function clear($key)  
    {  
        $cache_path = $this->_name($key);  
  
        if (file_exists($cache_path))  
        {  
            unlink($cache_path);  
            return TRUE;  
        }  
  
        return FALSE;  
    }  
  
    public function clearAll()  
    {  
        $dir = $this->dir;  
        if (is_dir($dir))   
        {  
            $dh=opendir($dir);  
  
            while (false !== ( $file = readdir ($dh)))   
            {  
 
                if($file!="." && $file!="..")   
                {   
                    $fullpath=$dir."/".$file;  
                    if(!is_dir($fullpath)) {  
                        unlink($fullpath);  
                    } else {  
                        delfile($fullpath);  
                    }
				
                }  
            }  
            closedir($dh);  
            // rmdir($dir);  
        } 

  }  
  
   private function _makeDir( $dir, $mode = "0777" ) {  
        if( ! $dir ) return 0;  
        $dir = str_replace( "\\", "/", $dir );  
      
        $mdir = "";  
        foreach( explode( "/", $dir ) as $val ) {  
          $mdir .= $val."/";  
          if( $val == ".." || $val == "." || trim( $val ) == "" ) continue;  
            
          if( ! file_exists( $mdir ) ) {  
            if(!@mkdir( $mdir, $mode )){  
             return false;  
            }  
          }  
        }  
        return true;  
    }  
}  
//去掉 javascript,去掉 HTML 标记,去掉空白字符,去掉空白字符,替换 HTML 实体;
$search = array("'<script[^>]*?>.*?</script>'si", "'<[\/\!]*?[^<>]*?>'si", "'([\r\n])[\s]+'", "'&(quot|#34);'i", "'&(amp|#38);'i", "'&(lt|#60);'i", "'&(gt|#62);'i", "'&(nbsp|#160);'i", "'&(iexcl|#161);'i", "'&(cent|#162);'i", "'&(pound|#163);'i", "'&(copy|#169);'i", "'&#(\d+);'e");
$replace = array("", "", "\\1", "\"", "&", "<", ">", " ", chr(161), chr(162), chr(163), chr(169), "chr(\\1)");

?>