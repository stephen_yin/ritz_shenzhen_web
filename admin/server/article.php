<?php

	include_once("public.php");
	switch ($port){
		
		case "getlistcount" :
			$type = (isset($_GET['type'])) ? $_GET['type'] : null ;
			$cate_id = (isset($_GET['cate_id'])) ? $_GET['cate_id'] : null ;
			$url = $site_domain.'article.getlistcount&type='.$type.'&cate_id='.$cate_id;
			$json_data = curlGet($url);
			print_r($json_data);			
			break;

		case "getlist" :
			$type = (isset($_GET['type'])) ? $_GET['type'] : null ;
			$cate_id = (isset($_GET['cate_id'])) ? $_GET['cate_id'] : null ;
			$from = (isset($_GET['from'])) ? $_GET['from'] : null;
			$nums = (isset($_GET['nums'])) ? $_GET['nums'] : null ;			
			$requestTime = date('Y-m-d H:i:s',$_SERVER['REQUEST_TIME']);//得到请求此php脚本时的时间戳
			$cache = new Cache();  
			$expiration = 8640;

			if($from != null){
				if($lang == "en") 	$json_data = $cache->get('article_en'.$cate_id.$from,$expiration);
				else				$json_data = $cache->get('article_cn'.$cate_id.$from,$expiration);
				
			}else{
				if($lang == "en") 	$json_data = $cache->get('article_en'.$cate_id,$expiration);
				else				$json_data = $cache->get('article_cn'.$cate_id,$expiration);
			}
	
			
			if ($json_data === FALSE){
				$url = $site_domain.'article.getlist&from='.$from.'&nums='.$nums.'&type='.$type.'&cate_id='.$cate_id;
				$json_data = curlGet($url);
				
			if($from != null){
				if($lang == "en")	$cache->set('article_en'.$cate_id.$from, $json_data); 
				else 				$cache->set('article_cn'.$cate_id.$from, $json_data); 
				
			}else{
				if($lang == "en")	$cache->set('article_en'.$cate_id, $json_data); 
				else 				$cache->set('article_cn'.$cate_id, $json_data); 
			}	
			file_put_contents('./log/log.txt','['.$requestTime.']--访问了一次article接口数据  | ',FILE_APPEND);			
			}		      
			print_r($json_data);					
			break;

		case "search" :
			$keywords = (isset($_GET['keywords'])) ? $_GET['keywords'] : null ;
			$key = urlencode($keywords);
			$url = $site_domain.'article.search&keywords='.$key;
			$json_data = curlGet($url);
			print_r($json_data);				

			break;

		case "getinfo" :
			$id = (isset($_GET['id'])) ? $_GET['id'] : null ;
			$url =$site_domain.'article.getinfo&id=' . $id;
			$json_data = curlGet($url);
			print_r($json_data);					
			break;

		case "fix" :
			$data = file_get_contents("php://input");//获取表单提交的数据;
			$cate_id = (isset($_GET['cate_id'])) ? $_GET['cate_id'] : null ;
			$from = (isset($_GET['page'])) ? $_GET['page'] : null;				
			$id = (isset($_GET['id'])) ? $_GET['id'] : null ;			
			$url = $site_domain.'article.fix&id=' . $id;	
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据
			$cache = new Cache(); 
			$cache->clearAll();			
			print_r($json_data);				
			break;

		case "add" :
			$data = file_get_contents("php://input");//获取表单提交的数据;
			$url = $site_domain.'article.add';
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据
			$cache = new Cache(); 
			$cache->clearAll();	
			print_r($json_data);			
			break;

		case "del" :
			$id = (isset($_GET['id'])) ? $_GET['id'] : null ;
			$url =$site_domain.'article.del&id=' . $id;
			$json_data = curlGet($url);
			$cache = new Cache(); 
			$cache->clearAll();	
			print_r($json_data);					
			break;	

		case "delall" :
			$id = (isset($_GET['id'])) ? $_GET['id'] : null ;
			$url =$site_domain.'article.delall&dellall=true';
			$json_data = curlGet($url);
			print_r($json_data);					
			break;	

		default:			
			print_r(json_encode($fail));
	}
		

?>