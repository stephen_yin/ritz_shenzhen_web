<?php

	
	include_once("public.php");
	
	switch ($port){


		case "getlist" :
				
			$id = (isset($_GET['id'])) ? $_GET['id'] : null ;
			$pid = (isset($_GET['pid'])) ? $_GET['pid'] : null ;					
			//根据id或上级id判断调用哪一级分类						
			if($pid !== null)		$url = $site_domain.'column.getlist&pid='.$pid;
			else if($id !== null)	$url = $site_domain.'column.getlist&id='.$id;
			else					$url = $site_domain.'column.getlist';	
			$requestTime = date('Y-m-d H:i:s',$_SERVER['REQUEST_TIME']);//得到请求此php脚本时的时间戳
			$cache = new Cache();  
			$expiration = 864000;
			if($lang == "en") $json_data = $cache->get('column_en',$expiration);
			else			  $json_data = $cache->get('column_cn',$expiration);
			if ($json_data === FALSE){
				
				$json_data = curlGet($url);
				if($lang == "en")	$cache->set('column_en', $json_data); 
				else 				$cache->set('column_cn', $json_data); 
				file_put_contents('log/log.txt','['.$requestTime.']--访问了一次接口数据  | ',FILE_APPEND);
			
			}		      
			print_r($json_data);				
			break;



		case "getinfo" :
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;
			$url =$site_domain.'column.getinfo&id=' . $id;			
			$json_data = curlGet($url);
			print_r($json_data);			
			break;



		case "fix" :
			$data = file_get_contents("php://input");//获取表单提交的数据;		
			$fixid = (isset($_GET['fixid'])) ? $_GET['fixid'] : false ;		
			$url = $site_domain.'column.fix&id=' . $fixid;			
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据
			$cache = new Cache(); 
			$cache->clearAll();			
			print_r($json_data);			
			break;




		case "add" :
			$data = file_get_contents("php://input");//获取表单提交的数据;
			$url = $site_domain.'column.add';			
			$json_data = curlPost($url, $data);//调用curlPost的方法，获取API返回的数据
			$cache = new Cache(); 
			$cache->clearAll();
			print_r($json_data);			
			break;


		case "disable" :
			$data ="cate_status=-1";
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;
			$url =$site_domain.'column.disable&id=' . $id;			
			$json_data = curlPost($url, $data);
			print_r($json_data);		
			break;				
			
			
		case "enable" :
			$data ="cate_status=1";
			$id = (isset($_GET['id'])) ? $_GET['id'] : false ;
			$url =$site_domain.'column.enable&id=' . $id;		
			$json_data = curlPost($url, $data);
			print_r($json_data);			
			break;

		default:
			
			print_r(json_encode($fail));
	}
		

?>