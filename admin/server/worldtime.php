<?php
include('public.php');
$port =(isset($_GET['port'])) ? $_GET['port'] : false ;

if($port == 'getlist'){
	date_default_timezone_set("Asia/shanghai");		
	$requestTime = date('Y-m-d H:i:s',$_SERVER['REQUEST_TIME']);//得到请求此php脚本时的时间戳
	$cache = new Cache();  
	$expiration = 600;
	if($lang == "en") $json_data = $cache->get('worldtimeen',$expiration);
	else			  $json_data = $cache->get('worldtime',$expiration);
	if ($json_data === FALSE){
		$url = $site_domain .  'worldtime.getlist';			
		$json_data = curlGet($url);
		if($lang == "en")	$cache->set('worldtimeen', $json_data); 
		else 				$cache->set('worldtime', $json_data); 
		file_put_contents('./log/log.txt','['.$requestTime.']--访问了一次worldtime接口数据  | ',FILE_APPEND);
	
	}		      
	print_r($json_data);
}

?>