var JsonType;
var mode;
var $_GET = (function() {
	var url = window.document.location.href.toString();
	
	var u = url.split("?");

	if (typeof (u[1]) == "string") {
		u = u[1].split("&");
		var get = {};

		for (var i in u) {
			var j = u[i].split("=");
			get[j[0]] = j[1];
		}

		return get;
	} else {
		return {};
	}
})();
//正则传输页面后面的参数；
//用法：GetQueryString("参数名1")；

function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');

    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
	

//获取URL中带参数后面的URL地址全部字符;
function getUrl(par){
var pos,str,para,parastr,parastr2,tempstr1,re_url;  
var tempstr="";  


 str = window.location.href;  
 pos = str.indexOf("?"+par)  

 parastr = str.substring(pos+1);  
re_url = parastr.indexOf("=");
parastr2= parastr.substring(re_url+1);
if (pos>0) return parastr2;
else return "";	
}



//过滤空格的函数

function removeHTMLTag(string) {
	if (string != ""){
		
		 string = string.replace(/<\/?[^>]*>/g,''); //去除HTML tag
         string = string.replace(/[ | ]*\n/g,'\n'); //去除行尾空白
       
         string=string.replace(/ /ig,'');//去掉 
	}
           
         return string;
    }


//判断是否是微信浏览器;
function isWeiXin(){ 
var ua = window.navigator.userAgent.toLowerCase(); 
if(ua.match(/MicroMessenger/i) == 'micromessenger'){ 
return true; 
}else{ 
return false; 
} 
} 

var api_key = 0;

//新增一家公司的api_key信息
	function addAK(ak, data_type, val){
		var obj = [];

		var data_array = new Object();

		data_array["ak"] = ak;
		data_array[data_type] = val;

		obj[0] = data_array;
		localStorage.token_data = JSON.stringify(obj);
	
	}


//token存储过程
	function setToken(ak, data_type, val){
		var str = localStorage.token_data;
		
		if(typeof(str) === "undefined" || str == ""){		//如果该设备中无相关ak信息注册时
			addAK(ak, data_type, val);
			return;
		}

		var obj = JSON.parse(str);
//		alert(obj.length);
		var cp = null;

		for(var i=0; i<obj.length; i++){
			if(obj[i]["ak"] == ak){
				cp = i;
				break;
			}
		}

		if(cp == null){			//如果该公司ak不存在时
			addAK(ak, data_type, val);
			return false;
		}

		obj[i][data_type] = val;

		localStorage.token_data = JSON.stringify(obj);
		
		
	}





//token获取
	function getToken(ak, data_type){
		var str = localStorage.token_data;
		

		if(typeof(str) === "undefined" || str == "") return "";

		var obj = JSON.parse(str);

		for(var i=0; i<obj.length; i++){
			if(obj[i]["ak"] == ak){
				return obj[i][data_type];
			}
		}

		return "";
	}
	
	
	
	
	
	

	
	
	
	
	
	
	


	

//字符串转金额
  function StrToMoney(str){
    var arr = str.split("");
	var hasPoint = false;

	var j = 0;

	for(var i=0; i<arr.length; i++){
		if(arr[i] == "."){
			hasPoint = true;
			continue;
		}

		if(hasPoint)
			j++;
	}

	if(j == 0) str += ".";
	for(; j<2; j++){
		str += "0";
	}

	return "￥" + str;
  }


//强制保留2位小数，如：2，会在2后面补上00.即2.00
	function toDecimal2(x){
		var f = parseFloat(x);
		if (isNaN(f)) return false;

		var f = Math.round(x*100)/100;
		var s = f.toString();
		var rs = s.indexOf('.');

		if (rs < 0){
			rs = s.length;
			s += '.';
		}

		while (s.length <= rs + 2){
			s += '0';
		}

		return s;
	}

	//检测本地文件存在与否
	function fileExists(URL) {  
	    var http = new XMLHttpRequest();  
	    http.open('HEAD', URL, false);  
	    http.send();
	    return http.status;  
//	    http.close();
	} 
	

//function OBJ(id,name){
//	this.food_id = id;
//	this.food_name =name
//}
//var arr=[];
//for(var i=0;i<10;i++){
//	arr.push(new OBJ("ID"+i,"名称"+i));
//}
//console.log(arr.length);